---

---

#### 1.AS的定义以及什麽時候需要BGP，什麽時候用静态路由，单宿主以及多宿主

* 多宿主/多路径/多出口 (1-64511 public | 64512-65535  private )
  * BGP is most appropriate when at least one of the following conditions exists:
    * An AS allow packets to transit through it to reach other autonomous systems(for example, it is a service provider)
    * An AS has  multiple connections to other autonomous systems
    * Routing policy and route selection for traffic entering and leaving the AS must be manipulated
  * BGP is not always appropriate. You do not have to use BGP if you have one of the following conditions:
    * Limited understanding of route filtering and BGP path-selection process
    * A single connection to the internet or another AS 
    * Lack of memory or process power to handle constant updates on BGP routers
* BGP 下一跳逻辑：BGP路由在一个AS內传递，下一跳地址不变;发送给其他AS的时候，才会将下一跳切换为边界路由器的更新源地址

* 自己产生BGP路由(始发路由) 
  * network(和IGP的network完全是两回事) 路由搬运工(路由通告)
  * Redistribute路由重发布
  * Aggregate 路由聚合/路由汇总  -- **默认产生的汇总/聚合路由下一跳为全0**
  * BGP表里就会存在所产生的BGP路由
    * 下一跳 0.0.0.0
    * 下一跳 继承 原来路由表中的下一跳会被完美继承
    * 如果本地BGP表里的下一跳全为0，无论通告给IBGP邻居还是EBGP邻居 下一跳都会变更为该路由器针对对应邻居的更新源地址
  * 如果本地BGP表里的路由下一跳继承原始下一跳 如果该路由通告给IBGP邻居 则下一跳不变 如果该路由通告给EBGP邻居 则下一跳改变为该路由器针对对应邻居的更新源地址
* 通过BGP邻居对等体接收BGP路由(穿越路由)
  * 接收到的BGP路由通常包含一个非0.0.0.0的下一跳地址 
    * 如果该路由通告给IBGP 下一跳不变 
    * 如果该路由通告给EBGP邻居 则下一跳变更为该路由器针对对应邻居的更新源地址\
    * 

#### 2.BGP的基本原理，以及BGP的路由下一跳规则

* 保证该**IBGP**路由的下一跳是可达的
* 保证该**IBGP**路由满足同步规则(synchronization) 收到一条IBGP路由 为了优选,加表,给邻居传递

#### 3.BGP的简单配置以及数据层面黑洞，以及解决方案

* 物理全互联(full mesh)  x	~~物理互联最不具备可扩展性~~
* 所有设备运行BGP  x
* Redistribute路由重发布  x	~~生产环境不可行~~
* 建立VPN Tunnel  x 	~~路由问题导致tunnel不停翻动~~
* 运行MPLS

#### 4.BGP邻接关系建立流程以及硬软重置

####  5.IBGP EBGP的防环规则

* **IBGP**

  * 收到一条IBGP路由，该路由不能传递给任何其他IBGP对等体，可以传递给EBGP对等体

  * 收到一条EBGP路由，该路由可以传递给任何BGP对等体

  * 始发的路由也可以传递给任何BGP对等体
* **EBGP**
  * AS-Path

#### 6.BGP软重置与BGP认证

* 建立TCP三次握手
* 彼此各交互一个open
* 彼此针对对方的open回复一个keepalived

##### BGP Status

1. `Idle`：Router is secharing routing rable to see whether a route exists to reach the neighbor

   > 1.IBGP路由有问题，无法到达邻居 2.neighbor 地址/ASnumber 指错
2. `Connect`：Router found a route to the neighbor and has competed the three-way TCP handshake
3. `Open sent`:：Open meaaage sent ,with the parameters for the BGP session
4. `Open Comfirm`：Route received arguement on the parameters for establishing session  --Alternatively ,router goes into `active`  state if no response to open message
5. `Established`：Peering is established;routing begins

##### BGP Established and Idle Status

* Idle：The router in this status cannot find the address of the neighbor in the routing table . cheak for an IGP problem. Is the neighbor announcing the route?
* Eatablished：The established status is the proper status for BGP operations. In the output of the `show ip bgp summary`  command, if the state column has a number, then the route is in the eatablished state. the number is  how many routes have been learned from this neighbor.

##### BGP Neighbor Authentication

* `neighbor {ip-address | peer-group-names} passwd string`
  * BGP authentication uses MD5
  * Configure a key (password); router generates a message digest. or hash, of the key and the message.
  * Message digest is sent; key is not sent.
  * Router generates and checks the MD5 digest of every segment sent on TCP connection.Router authenticates the source of each routing update packet that it receives 

##### Clear the BGP Session --BGP增量型更新

* When policies such as access lists or attributes are changed, the change takes effect immediately, and the next time that a prefix or path is advertised or received, the new policy is used. it can take a long time for the policy to be applied to all networks.
* You must trigger an update to ensure that the policy is immediately to all affected prefixes and paths
* Ways to trigger and update
  * Hard reset  --硬清  `clear ip bgp {*|neighbor}`  --TCP三次握手中断  --重新建立
  * Soft reset  --` clear ip bgp {*|neighbor} soft in|out ` 
  * Route refresh

> 路由协议更新方式：周期性更新|触发式更新 

> BFP额外的2张表 ；本地路由器通过邻居学到了那些BGP路由；本地路由器即将给邻居通告的所有BGP路由



##### 建立了BGP邻居，无法正常接收到邻居发送的路由？

**Inbound Soft Reset**

`neighbor [ip-address] soft-reconfiguration inbound`

* This router stores all updates from this neighbor in case the inbound policy is changed
* The command is memory-intensive

`clear ip bgp {*|ip-address} soft in`

*  Uses the stored information to generate new inbound updates



#### BGP TABLE

* **Neithbor table**
  * **LIst of BGP neighbors**
* **BGP table(forwarding database)**
  * **list of all networks learned from each neighbor**
  * **can contain multiple paths to destination networks**
  * **contains BGP attributes for each path**
* **IP routing table**
  * **list of best paths to destination networks**

#### BGP defines the following message types:

* **Open**
  * **includes hold time and BGP router ID**
* **Keepalive**
* **UPdate**
  * **information for one path only(cloud be to multiple networks)**
  * **include path attributes and network**
* **Notification**
  * **when error is detected**
  * **BGP connection closed after message is sent**
* **Route-Refresh**

#### BGP的配置以及特殊MA网络的下一跳规则

* 一台路由器去往上游BGP邻居的下一跳地址如果和去往下游BGP邻居的下一跳地址在同一个IP子网段内，将路由的下一跳设置为去往上游邻居的下一跳地址

* **Example：Next Hop on a Multiaccess Network**

![]($(pwd)/bird-bpg/next hop.png)

* **The fllowing takes place in a multiaccess network:**

  * Router B advertises network 172.30.0.0 to router A in EBGP with a next hop of 10.10.10.2,not 10.10.10.1. This avoids an unnecessary hop.

  * BGP is being efficient by informing AS 64520 of the best entry point into AS 65000 for network 172.30.0.0.

  * Router B in AS 65000 also advertises to AS 64520 that the best entry point for each network in AS 64600 is the next hop of router C because that is the best path to move through AS 65000 to AS 64600.
